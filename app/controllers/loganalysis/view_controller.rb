#encoding=utf-8
class Loganalysis::ViewController < Loganalysis::BaseController
  def index
    @day = DateUtils.today-1.day

    unless params[:day].nil?
      @day = DateUtils.format_day params[:day]
    end

    view_count_top_10_level0 = LogViewInfo.where(:day_date=>@day, :level=>0).order_by(:count, :desc).limit(10)

    view_count_top_10_level1 = LogViewInfo.where(:day_date=>@day, :level=>1).order_by(:count, :desc).limit(10)

    view_count_top_10_level2 = LogViewInfo.where(:day_date=>@day, :level=>2).order_by(:count, :desc).limit(10)

    view_count_top_10_level3 = LogViewInfo.where(:day_date=>@day, :level=>3).order_by(:count, :desc).limit(10)

    view_count_top_10_level4 = LogViewInfo.where(:day_date=>@day, :level=>4).order_by(:count, :desc).limit(10)

    view_count_top_10_level5 = LogViewInfo.where(:day_date=>@day, :level=>5).order_by(:count, :desc).limit(10)

    view_count_top_10_level6 = LogViewInfo.where(:day_date=>@day, :level=>6).order_by(:count, :desc).limit(10)

    view_count_top_10_level7 = LogViewInfo.where(:day_date=>@day, :level=>7).order_by(:count, :desc).limit(10)

    count_top_10_categories_level0 = []

    count_top_10_data_level0 = []

    view_count_top_10_level0.each do |info|
      count_top_10_categories_level0<<DataUtils.concat_string(info.province, info.city)
      count_top_10_data_level0<<info.count
    end

    @view_count_level0_top_10_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"0层view查询次数 TOP 10"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= count_top_10_categories_level0
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      f.series(:name=>'0层view查询次数', :data=>count_top_10_data_level0)
    end

    count_top_10_categories_level1 = []

    count_top_10_data_level1 = []

    view_count_top_10_level1.each do |info|
      count_top_10_categories_level1<<DataUtils.concat_string(info.province, info.city)
      count_top_10_data_level1<<info.count
    end

    @view_count_level1_top_10_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"1层view查询次数 TOP 10"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= count_top_10_categories_level1
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      f.series(:name=>'1层view查询次数', :data=>count_top_10_data_level1)
    end

    count_top_10_categories_level2 = []

    count_top_10_data_level2 = []

    view_count_top_10_level2.each do |info|
      count_top_10_categories_level2<<DataUtils.concat_string(info.province, info.city)
      count_top_10_data_level2<<info.count
    end

    @view_count_level2_top_10_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"2层view查询次数 TOP 10"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= count_top_10_categories_level2
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      f.series(:name=>'2层view查询次数', :data=>count_top_10_data_level2)
    end

    count_top_10_categories_level3 = []

    count_top_10_data_level3 = []

    view_count_top_10_level3.each do |info|
      count_top_10_categories_level3<<DataUtils.concat_string(info.province, info.city)
      count_top_10_data_level3<<info.count
    end

    @view_count_level3_top_10_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"3层view查询次数 TOP 10"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= count_top_10_categories_level3
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      f.series(:name=>'3层view查询次数', :data=>count_top_10_data_level3)
    end

    count_top_10_categories_level4 = []

    count_top_10_data_level4 = []

    view_count_top_10_level4.each do |info|
      count_top_10_categories_level4<<DataUtils.concat_string(info.province, info.city)
      count_top_10_data_level4<<info.count
    end

    @view_count_level4_top_10_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"4层view查询次数 TOP 10"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= count_top_10_categories_level4
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      f.series(:name=>'4层view查询次数', :data=>count_top_10_data_level4)
    end

    count_top_10_categories_level5 = []

    count_top_10_data_level5 = []

    view_count_top_10_level5.each do |info|
      count_top_10_categories_level5<<DataUtils.concat_string(info.province, info.city)
      count_top_10_data_level5<<info.count
    end

    @view_count_level5_top_10_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"5层view查询次数 TOP 10"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= count_top_10_categories_level5
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      f.series(:name=>'5层view查询次数', :data=>count_top_10_data_level5)
    end

    count_top_10_categories_level6 = []

    count_top_10_data_level6 = []

    view_count_top_10_level6.each do |info|
      count_top_10_categories_level6<<DataUtils.concat_string(info.province, info.city)
      count_top_10_data_level6<<info.count
    end

    @view_count_level6_top_10_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"6层view查询次数 TOP 10"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= count_top_10_categories_level6
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      f.series(:name=>'6层view查询次数', :data=>count_top_10_data_level6)
    end

    count_top_10_categories_level7 = []

    count_top_10_data_level7 = []

    view_count_top_10_level7.each do |info|
      count_top_10_categories_level7<<DataUtils.concat_string(info.province, info.city)
      count_top_10_data_level7<<info.count
    end

    @view_count_level7_top_10_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"7层view查询次数 TOP 10"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= count_top_10_categories_level7
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      f.series(:name=>'7层view查询次数', :data=>count_top_10_data_level7)
    end
  end

  #更多详细信息分页
  def view_details_page
    @day = DateUtils.today-1.day

    @level = params[:level]

    @page_info = LogViewInfo.where(:day_date=>@day, :level=>@level.to_i).order_by(:count, :desc).paginate(:per_page => 20, :page=>params[:page])

  end
end

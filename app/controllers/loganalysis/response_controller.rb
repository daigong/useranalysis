#encoding=utf-8
class Loganalysis::ResponseController < Loganalysis::BaseController

  def index

    @day = DateUtils.today-1.day

    unless params[:day].nil?
      @day = DateUtils.format_day params[:day]
    end

    response_time_info=ResponseTimeInfo.get_day_info @day

    mintime_data = response_time_info["min"]

    avgtime_data = response_time_info["avg"]

    maxtime_data = response_time_info["max"]

    count_data = response_time_info["count"]


    rg_view_response_time_info=ResponseTimeInfo.get_rg_view_day_info @day

    rg_mintime_data = rg_view_response_time_info["rg"]["min"]

    rg_avgtime_data = rg_view_response_time_info["rg"]["avg"]

    rg_maxtime_data = rg_view_response_time_info["rg"]["max"]

    rg_count_data = rg_view_response_time_info["rg"]["count"]

    view_mintime_data = rg_view_response_time_info["view"]["min"]

    view_avgtime_data = rg_view_response_time_info["view"]["avg"]

    view_maxtime_data = rg_view_response_time_info["view"]["max"]

    view_count_data = rg_view_response_time_info["view"]["count"]


    @count_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"统计次数"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= count_data.keys
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      f.series(:name=>'统计次数', :data=>count_data.values)
    end


    @mintime_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"最小响应时间"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= mintime_data.keys
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      f.series(:name=>'最小响应时间', :data=>mintime_data.values)
    end

    @avgtime_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"平均响应时间"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= avgtime_data.keys
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      f.series(:name=>'平均响应时间', :data=>avgtime_data.values)
    end

    @maxtime_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"最大响应时间"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= maxtime_data.keys
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      f.series(:name=>'最大响应时间', :data=>maxtime_data.values)
    end

    @rg_count_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"统计次数"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= rg_count_data.keys
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      f.series(:name=>'统计次数', :data=>rg_count_data.values)
    end

    @rg_mintime_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"最小响应时间"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= rg_mintime_data.keys
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      f.series(:name=>'最小响应时间', :data=>rg_mintime_data.values)
    end

    @rg_avgtime_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"平均响应时间"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= rg_avgtime_data.keys
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      f.series(:name=>'平均响应时间', :data=>rg_avgtime_data.values)
    end

    @rg_maxtime_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"最大响应时间"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= rg_maxtime_data.keys
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      f.series(:name=>'最大响应时间', :data=>rg_maxtime_data.values)
    end


    @view_count_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"统计次数"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= view_count_data.keys
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      f.series(:name=>'统计次数', :data=>view_count_data.values)
    end

    @view_mintime_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"最小响应时间"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= view_mintime_data.keys
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      f.series(:name=>'最小响应时间', :data=>view_mintime_data.values)
    end

    @view_avgtime_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"平均响应时间"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= view_avgtime_data.keys
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      f.series(:name=>'平均响应时间', :data=>view_avgtime_data.values)
    end

    @view_maxtime_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"最大响应时间"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= view_maxtime_data.keys
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      f.series(:name=>'最大响应时间', :data=>view_maxtime_data.values)
    end


  end
end

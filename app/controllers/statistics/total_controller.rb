#encoding=utf-8
class Statistics::TotalController < ApplicationController

  layout 'statistics/layout_no_menu'

  def index

    @day = DateUtils.today - 1.day

    @begin_day = @day-20.day

    @end_day = @day

    @today_total_count = {
        :new_user_count=>BaseInfo.where(:day_date=>@day).sum(:new_user_count).to_i,
        :login_user_count=>BaseInfo.where(:day_date=>@day).sum(:login_user_count).to_i,
        :login_count=>BaseInfo.where(:day_date=>@day).sum(:login_count).to_i
    }

    @yesterday_total_count ={
        :new_user_count=>BaseInfo.where(:day_date=>@day-1.day).sum(:new_user_count).to_i,
        :login_user_count=>BaseInfo.where(:day_date=>@day-1.day).sum(:login_user_count).to_i,
        :login_count=>BaseInfo.where(:day_date=>@day-1.day).sum(:login_count).to_i
    };

    total_count_except_today = BaseInfo.where(:day_date=>@day).sum(:sum_user_count).to_i
    @total_count = total_count_except_today + BaseInfo.where(:day_date=>@day).sum(:new_user_count).to_i

    @every_day_infos = BaseInfo.get_total_by_day @begin_day, @end_day

    @new_user_count_data = [];
    @login_user_count_data = [];
    @login_count_data = [];

    @every_day_infos.each do |info|
      @new_user_count_data<<info[:new_user_count]
      @login_user_count_data<<info[:login_user_count]
      @login_count_data<<info[:login_count]
    end

    @new_user_count_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"每日新增用户"};
      f.options[:chart][:width]=950
      f.options[:chart][:height]=300
      f.options[:chart][:defaultSeriesType] = "spline"
      f.options[:xAxis][:categories]= DateUtils.get_day_string(@begin_day, @end_day)
      f.series(:name=>'数量', :data=>@new_user_count_data)
    end

    @login_user_count_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"每日启动用户"};
      f.options[:chart][:width]=950
      f.options[:chart][:height]=300
      f.options[:chart][:defaultSeriesType] = "spline"
      f.options[:xAxis][:categories]= DateUtils.get_day_string(@begin_day, @end_day)
      f.series(:name=>'数量', :data=>@login_user_count_data)
    end

    @login_count_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"每日启动次数"};
      f.options[:chart][:width]=950
      f.options[:chart][:height]=300
      f.options[:chart][:defaultSeriesType] = "spline"
      f.options[:xAxis][:categories]= DateUtils.get_day_string(@begin_day, @end_day)
      f.series(:name=>'数量', :data=>@login_count_data)
    end

    @productions_page_info = Product.where(:is_active=>true).order_by(:name,:desc).paginate(:per_page => 10,:page=>params[:page]);
  end
end

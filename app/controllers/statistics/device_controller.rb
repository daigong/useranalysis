#encoding=utf-8

class Statistics::DeviceController < Statistics::BaseController
  def index

    @day = DateUtils.today - 1.day

    unless params[:day].nil?
      @day = DateUtils.format_day params[:day]
    end

    @new_user_count_top_10 = DeviceInfo.where(:key=>params[:app_id], :day_date=>@day).order_by(:new_user_count, :desc).limit(10)

    @sum_user_count_top_10 = DeviceInfo.where(:key=>params[:app_id], :day_date=>@day).order_by(:login_user_count, :desc).limit(10)

    @new_user_count_top_10_categories = []

    @new_user_count_top_10_data = []

    @new_user_count_top_10.each do |device_info|
      device_info.device_model="Unknown" if device_info.device_model.blank?
      @new_user_count_top_10_categories<<device_info.device_model
      @new_user_count_top_10_data<<device_info.new_user_count
    end

    @sum_user_count_top_10_categories = []

    @sum_user_count_top_10_data = []

    @sum_user_count_top_10.each do |device_info|
      device_info.device_model="Unknown" if device_info.device_model.blank?
      @sum_user_count_top_10_categories<<device_info.device_model
      @sum_user_count_top_10_data<<device_info.login_user_count
    end

    @sum_user_count_device_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"活跃用户 设备 TOP 10"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= @sum_user_count_top_10_categories
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      f.series(:name=>'活跃', :data=>@sum_user_count_top_10_data)
    end

    @new_user_count_device_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"新增用户 设备 TOP 10"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= @new_user_count_top_10_categories
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      f.series(:name=>'新增', :data=>@new_user_count_top_10_data)
    end

    @page_info = DeviceInfo.where(:key=>params[:app_id], :day_date=>@day, :login_user_count.exists=>true).
        order_by(:login_user_count, :desc).paginate(:per_page => 5)

  end

  #设备详细信息分页
  def device_details_page
    @day = DateUtils.today-1.day

    unless params[:day].nil?
      @day = DateUtils.format_day params[:day]
    end

    @page_info = DeviceInfo.where(:key=>params[:app_id], :day_date=>@day, :login_user_count.exists=>true).
        order_by(:login_user_count, :desc).paginate(:per_page => 20, :page=>params[:page]);
  end
end

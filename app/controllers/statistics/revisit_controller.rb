#encoding=utf-8
#回访用户统计
class Statistics::RevisitController < Statistics::BaseController
  def index

    @day=DateUtils.today-1.day
    #统计14天本身需要-15.day,我们显示15天还需要减15.day
    @begin_day=@day-15.day-15.day

    @end_day=@day

    @revisit_info=RevisitInfo.where(:key=>params[:app_id], :day_date.gte=>@begin_day, :day_date.lte=>@end_day).
        order_by(:day_date, :asc)

    @date_string = [];

    @revisit_info_7_counts = [];

    @revisit_info_14_counts = [];

    @new_user__counts = [];


    @revisit_info.each do |info|
      @date_string<<DateUtils.format_date_to_string(info.day_date)
      @revisit_info_7_counts<<info.week_user_count
      @revisit_info_14_counts<<info.dweek_user_count
      @new_user__counts<<info.new_user_count
    end

    @line_7_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"7天内回访"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "line"
      f.options[:xAxis][:categories]= @date_string
      f.options[:xAxis][:labels]={"step" =>3, "align" =>"right", "rotation" => -90}
      f.series(:name=>'7天内回访用户', :data=>@revisit_info_7_counts, :type=>"area")
      f.series(:name=>'新增用户', :data=>@new_user__counts)
      f.options[:legend][:layout]=:horizontal
    end

    @line_14_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"14天内回访"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "line"
      f.options[:xAxis][:categories]= @date_string
      f.options[:xAxis][:labels]={"step" =>3, "align" =>"right", "rotation" => -90}
      f.series(:name=>'14天内回访用户', :data=>@revisit_info_14_counts, :type=>"area")
      f.series(:name=>'新增用户', :data=>@new_user__counts)
      f.options[:legend][:layout]=:horizontal
    end

    @percent_7_counts = []

    @percent_14_counts = []

    @revisit_info.each do |info|
      @percent_7_counts<< CalcUtils.calc_percentage_to_i(info.week_user_count, info.new_user_count, 0);
      @percent_14_counts<< CalcUtils.calc_percentage_to_i(info.dweek_user_count, info.new_user_count, 0);
    end

    @revisit_percent_7_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"7天回访比例"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "line"
      f.options[:xAxis][:categories]= @date_string
      f.options[:xAxis][:labels]={"step" =>3, "align" =>"right", "rotation" => -90}
      f.series(:name=>'今日', :data=>@percent_7_counts, :type=>"area")
    end

    @revisit_percent_14_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"14天回访比例"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "line"
      f.options[:xAxis][:categories]= @date_string
      f.options[:xAxis][:labels]={"step" =>3, "align" =>"right", "rotation" => -90}
      f.series(:name=>'今日', :data=>@percent_14_counts, :type=>"area")
    end

    @page_info = RevisitInfo.where(:key=>params[:app_id]).
        order_by(:day_date, :desc).paginate(:per_page => 15)
  end

  def daily_revisit_details_page
    
    @day = DateUtils.today-1.day

    unless params[:day].nil?
      @day = DateUtils.format_day params[:day]
    end

    @page_info =RevisitInfo.where(:key=>params[:app_id]).order_by(:day_date, :desc).paginate(:per_page => 20,:page=>params[:page]);
    
  end
end

#encoding=utf-8
class Statistics::LaunchController < Statistics::BaseController
  def index

    @day = DateUtils.today-1.day

    @begin_day = @day-15.day
    @end_day = @day

    @login_count_info = LoginCountInfo.where(:key=>params[:app_id], :day_date.gte=>@begin_day, :day_date.lte=>@end_day).
        order_by(:day_date, :asc)

    @level_0_counts = []
    @level_1_counts = []
    @level_2_counts = []
    @level_3_counts = []
    @level_4_counts = []
    @level_5_counts = []

    @login_count_info.each do |info|
      @level_0_counts<<info.login_count_level0
      @level_1_counts<<info.login_count_level1
      @level_2_counts<<info.login_count_level2
      @level_3_counts<<info.login_count_level3
      @level_4_counts<<info.login_count_level4
      @level_5_counts<<info.login_count_level5
    end

    @launch_count_distribution_column_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"日启动次数分布"};
      f.options[:chart][:width]=700
      f.options[:xAxis][:categories]= DateUtils.get_day_string(@begin_day, @end_day);
      f.options[:chart][:defaultSeriesType] = "spline"
      f.options[:xAxis][:labels]={"step" =>2, "align" =>"right", "rotation" => -90}
      f.series(:name=>'1-2次', :data=>@level_0_counts)
      f.series(:name=>'3-5次', :data=>@level_1_counts)
      f.series(:name=>'6-9次', :data=>@level_2_counts)
      f.series(:name=>'10-19次', :data=>@level_3_counts)
      f.series(:name=>'20-49次', :data=>@level_4_counts)
      f.series(:name=>'50+次', :data=>@level_5_counts)
      f.options[:yAxis][:min]= 0
      f.options[:legend][:layout]=:horizontal
    end

    @today_count_info = LoginCountInfo.where(:key=>params[:app_id], :day_date=>@day).first

    @total = @today_count_info.login_count_level0+@today_count_info.login_count_level1+
        @today_count_info.login_count_level2+ @today_count_info.login_count_level3+
        @today_count_info.login_count_level4+@today_count_info.login_count_level5

    @launch_count_distribution_bar_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"日启动次数分布"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= ['1-2次', '3-5次', '6-9次', '10-19次', '20-49次', '50+次']
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      counts = [
          CalcUtils.calc_percentage_to_i(@today_count_info.login_count_level0, @total, 1),
          CalcUtils.calc_percentage_to_i(@today_count_info.login_count_level1, @total, 1),
          CalcUtils.calc_percentage_to_i(@today_count_info.login_count_level2, @total, 1),
          CalcUtils.calc_percentage_to_i(@today_count_info.login_count_level3, @total, 1),
          CalcUtils.calc_percentage_to_i(@today_count_info.login_count_level4, @total, 1),
          CalcUtils.calc_percentage_to_i(@today_count_info.login_count_level5, @total, 1),
      ]
      f.series(:name=>"#{DateUtils.format_date_to_string(@day)}", :data=>counts)
    end

    week_begin_day = @day - 6.day

    week_login_count_info = LoginCountInfo.where(:key=>params[:app_id], :day_date.gte=>week_begin_day, :day_date.lte=>@end_day)

    week_level_0_counts = 0
    week_level_1_counts = 0
    week_level_2_counts = 0
    week_level_3_counts = 0
    week_level_4_counts = 0
    week_level_5_counts = 0

    week_login_count_info.each do |info|
      week_level_0_counts+=info.login_count_level0
      week_level_1_counts+=info.login_count_level1
      week_level_2_counts+=info.login_count_level2
      week_level_3_counts+=info.login_count_level3
      week_level_4_counts+=info.login_count_level4
      week_level_5_counts+=info.login_count_level5
    end

    week_total = week_level_0_counts+week_level_1_counts+week_level_2_counts+week_level_3_counts+week_level_4_counts+week_level_5_counts

    @week_launch_count_distribution_bar_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"周启动次数分布"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= ['1-2次', '3-5次', '6-9次', '10-19次', '20-49次', '50+次']
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      @week_counts = [
          CalcUtils.calc_percentage_to_i(week_level_0_counts, week_total, 1),
          CalcUtils.calc_percentage_to_i(week_level_1_counts, week_total, 1),
          CalcUtils.calc_percentage_to_i(week_level_2_counts, week_total, 1),
          CalcUtils.calc_percentage_to_i(week_level_3_counts, week_total, 1),
          CalcUtils.calc_percentage_to_i(week_level_4_counts, week_total, 1),
          CalcUtils.calc_percentage_to_i(week_level_5_counts, week_total, 1),
      ]
      f.series(:name=>"#{DateUtils.format_date_to_string(week_begin_day)}～#{DateUtils.format_date_to_string(@day)}", :data=>@week_counts)
    end

    month_begin_day = @day - 31.day

    month_login_count_info = LoginCountInfo.where(:key=>params[:app_id], :day_date.gte=>month_begin_day, :day_date.lte=>@end_day)

    month_level_0_counts = 0
    month_level_1_counts = 0
    month_level_2_counts = 0
    month_level_3_counts = 0
    month_level_4_counts = 0
    month_level_5_counts = 0

    month_login_count_info.each do |info|
      month_level_0_counts+=info.login_count_level0
      month_level_1_counts+=info.login_count_level1
      month_level_2_counts+=info.login_count_level2
      month_level_3_counts+=info.login_count_level3
      month_level_4_counts+=info.login_count_level4
      month_level_5_counts+=info.login_count_level5
    end

    month_total = month_level_0_counts+month_level_1_counts+month_level_2_counts+month_level_3_counts+month_level_4_counts+month_level_5_counts

    @month_launch_count_distribution_bar_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"月启动次数分布"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= ['1-2次', '3-5次', '6-9次', '10-19次', '20-49次', '50+次']
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      @month_counts = [
          CalcUtils.calc_percentage_to_i(month_level_0_counts, month_total, 1),
          CalcUtils.calc_percentage_to_i(month_level_1_counts, month_total, 1),
          CalcUtils.calc_percentage_to_i(month_level_2_counts, month_total, 1),
          CalcUtils.calc_percentage_to_i(month_level_3_counts, month_total, 1),
          CalcUtils.calc_percentage_to_i(month_level_4_counts, month_total, 1),
          CalcUtils.calc_percentage_to_i(month_level_5_counts, month_total, 1),
      ]
      f.series(:name=>"#{DateUtils.format_date_to_string(month_begin_day)}～#{DateUtils.format_date_to_string(@day)}", :data=>@month_counts)
    end
  end
end

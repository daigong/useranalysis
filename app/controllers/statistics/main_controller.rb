#encoding=utf-8
class Statistics::MainController < Statistics::BaseController


  def index

    @day = DateUtils.today - 1.day
    #######################基本统计#############################
    @today_result = BaseInfo.get_day_info params[:app_id], @day;


    @yesterday_result =BaseInfo.get_day_info params[:app_id], @day-1.day;

    @total_user_count=@today_result[:sum_user_count]


    #######################时段分析#############################
    #
    #@installations_by_hours_chart = LazyHighCharts::HighChart.new('line') do |f|
    #  f.options[:title]={:text=>"新增用户时段图表"};
    #  f.options[:chart][:width]=700
    #  f.options[:chart][:defaultSeriesType] = "spline"
    #  f.options[:xAxis][:categories]= (0...24).to_a
    #  f.options[:xAxis][:labels]={"step" =>2, "align" =>"right"}
    #  f.series(:name=>'今日', :data=>BaseInfo.new_user_count_by_hour_info(@day, params[:app_id]))
    #  f.series(:name=>'昨日', :data=>BaseInfo.new_user_count_by_hour_info(@day-1.day, params[:app_id]), :visible=>false)
    #  #f.series(:name=>'7天前', :data=>BaseInfo.new_user_count_by_hour_info(@day-7.day, params[:app_id]), :visible=>false)
    #  #f.series(:name=>'30天前', :data=>BaseInfo.new_user_count_by_hour_info(@day-30.day, params[:app_id]), :visible=>false)
    #end
    #@launches_by_hours_chart = LazyHighCharts::HighChart.new('line') do |f|
    #  f.options[:title]={:text=>"启动次数时段图表"};
    #  f.options[:chart][:width]=700
    #  f.options[:chart][:defaultSeriesType] = "spline"
    #  f.options[:xAxis][:categories]= (0...24).to_a
    #  f.options[:xAxis][:labels]={"step" =>2, "align" =>"right"}
    #  f.series(:name=>'今日', :data=>BaseInfo.login_count_by_hour_info(@day, params[:app_id]))
    #  f.series(:name=>'昨日', :data=>BaseInfo.login_count_by_hour_info(@day-1.day, params[:app_id]), :visible=>false)
    #  #f.series(:name=>'7天前', :data=>BaseInfo.login_count_by_hour_info(@day-7.day, params[:app_id]), :visible=>false)
    #  #f.series(:name=>'30天前', :data=>BaseInfo.login_count_by_hour_info(@day-30.day, params[:app_id]), :visible=>false)
    #end
    #######################趋势分析####################################
    @begin_day = @day-15.day
    @end_day =@day

    base_infos = BaseInfo.get_by_day(@begin_day, @end_day, params[:app_id])
    #每日新用户趋势
    new_user_count_infos = [];
    #累计用户
    sum_user_count_infos = [];
    #启动用户
    login_user_count_infos = [];
    #启动次数
    login_count_infos = [];
    #平均使用时长
    average_time_infos = [];
    #累计登录次数
    old_user_count_infos=[];
    #平均上传流量
    average_traffics_ups=[];
    #平均下载流量
    average_traffics_downs=[];
    base_infos.each do |base_info|
      unless base_info.nil?
        new_user_count_infos<<base_info.new_user_count
        sum_user_count_infos<<base_info.sum_user_count
        login_user_count_infos<<base_info.login_user_count
        login_count_infos<<base_info.login_count
        old_user_count_infos<<base_info.login_user_count-base_info.new_user_count
        if base_info['average_use_time'].nil?
          average_time_infos<<0
        else
          average_time_infos<<base_info['average_use_time']
        end
        if base_info['average_traffics_up'].nil?
          average_traffics_ups<<0
        else
          average_traffics_ups<<base_info['average_traffics_up']
        end
        if base_info['average_traffics_down'].nil?
          average_traffics_downs<<0
        else
          average_traffics_downs<<base_info['average_traffics_down']
        end
      else
        new_user_count_infos<<0
        sum_user_count_infos<<sum_user_count_infos.last
        login_user_count_infos<<0
        login_count_infos<<0
        old_user_count_infos<<old_user_count_infos.last
        average_time_infos<<0
        average_traffics_ups<<0
        average_traffics_downs<<0
      end
    end

    @installations_by_daily_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"每日新用户趋势"};
      f.options[:chart][:width]=700
      f.options[:chart][:height]=300
      f.options[:chart][:defaultSeriesType] = "spline"
      f.options[:xAxis][:categories]= DateUtils.get_day_string(@begin_day, @end_day)
      f.options[:xAxis][:labels]={"step" =>3}
      f.series(:name=>'每日新用户趋势', :data=>new_user_count_infos)
    end

    @total_installations_by_daily_chart= LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"累计用户"};
      f.options[:chart][:width]=700
      f.options[:chart][:height]=300
      f.options[:chart][:defaultSeriesType] = "column"
      f.options[:xAxis][:categories]= DateUtils.get_day_string(@begin_day, @end_day)
      f.options[:xAxis][:labels]={"step" =>3}
      f.series(:name=>'累计用户', :data=>sum_user_count_infos)
    end

    @active_users_split_by_daily_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"启动用户"};
      f.options[:chart][:width]=700
      f.options[:chart][:height]=300
      f.options[:chart][:defaultSeriesType] = "column"
      f.options[:xAxis][:categories]= DateUtils.get_day_string(@begin_day, @end_day)
      f.options[:xAxis][:labels]={"step" =>3}
      f.series(:name=>'新用户', :data=>new_user_count_infos)
      f.series(:name=>'老用户', :data=>old_user_count_infos)
      f.options[:plotOptions]={"column"=>{"stacking"=>"normal"}}
    end

    @launches_by_daily_chart=LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"每日启动次数"};
      f.options[:chart][:width]=700
      f.options[:chart][:height]=300
      f.options[:chart][:defaultSeriesType] = "spline"
      f.options[:xAxis][:categories]= DateUtils.get_day_string(@begin_day, @end_day)
      f.options[:xAxis][:labels]={"step" =>3}
      f.series(:name=>'每日启动次数', :data=>login_count_infos)
    end

    @average_time_by_daily_chart=LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"平均使用时长"};
      f.options[:chart][:width]=700
      f.options[:chart][:height]=300
      f.options[:chart][:defaultSeriesType] = "spline"
      f.options[:xAxis][:categories]= DateUtils.get_day_string(@begin_day, @end_day)
      f.options[:xAxis][:labels]={"step" =>3}
      f.series(:name=>'平均使用时长', :data=>average_time_infos)
    end

    @average_traffics_up_by_daily_chart=LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"平均上传流量"};
      f.options[:chart][:width]=700
      f.options[:chart][:height]=300
      f.options[:chart][:defaultSeriesType] = "spline"
      f.options[:xAxis][:categories]= DateUtils.get_day_string(@begin_day, @end_day)
      f.options[:xAxis][:labels]={"step" =>3}
      f.series(:name=>'平均上传流量', :data=>average_traffics_ups)
    end

    @average_traffics_down_by_daily_chart=LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"平均下载流量"};
      f.options[:chart][:width]=700
      f.options[:chart][:height]=300
      f.options[:chart][:defaultSeriesType] = "spline"
      f.options[:xAxis][:categories]= DateUtils.get_day_string(@begin_day, @end_day)
      f.options[:xAxis][:labels]={"step" =>3}
      f.series(:name=>'平均下载流量', :data=>average_traffics_downs)
    end

    ########################每日明细####################################
    #该应用第一天有数据
    @page_info = BaseInfo.where(:key=>params[:app_id]).order_by(:day_date, :desc).paginate(:per_page => 5, :page=>params[:page])
  end

  #每日明细_分页信息
  def daily_stats_details_page
    @page_info = BaseInfo.where(:key=>params[:app_id]).order_by(:day_date, :desc).paginate(:per_page => 20, :page=>params[:page])
  end


  def total_active_info_page

    @day = DateUtils.today-1.day

    @today_result = BaseInfo.get_day_info params[:app_id], @day;
    @total_launches_count = @today_result[:sum_login_count]

    @total_user_count=@today_result[:sum_user_count]

    render :layout => nil
  end
end

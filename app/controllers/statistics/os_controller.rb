#encoding=utf-8
class Statistics::OsController < Statistics::BaseController
  def index

    @day = DateUtils.today - 1.day

    unless params[:day].nil?
      @day = DateUtils.format_day params[:day]
    end

    @new_user_count_top_10 = OsVersionInfo.where(:key=>params[:app_id], :day_date=>@day).order_by(:new_user_count, :desc).limit(10)

    @sum_user_count_top_10 = OsVersionInfo.where(:key=>params[:app_id], :day_date=>@day).order_by(:login_user_count, :desc).limit(10)

    @new_user_count_top_10_categories = []

    @new_user_count_top_10_data = []

    @new_user_count_top_10.each do |info|
      info.os_version="Unknown" if info.os_version.blank?
      @new_user_count_top_10_categories<<info.os_version
      @new_user_count_top_10_data<<info.new_user_count
    end

    @sum_user_count_top_10_categories = []

    @sum_user_count_top_10_data = []

    @sum_user_count_top_10.each do |info|
      info.os_version="Unknown" if info.os_version.blank?
      @sum_user_count_top_10_categories<<info.os_version
      @sum_user_count_top_10_data<<info.login_user_count
    end

    @sum_user_count_device_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"活跃用户 操作系统 TOP 10"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= @sum_user_count_top_10_categories
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      f.series(:name=>'活跃', :data=>@sum_user_count_top_10_data)
    end

    @new_user_count_device_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"新增用户 操作系统 TOP 10"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= @new_user_count_top_10_categories
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      f.series(:name=>'新增', :data=>@new_user_count_top_10_data)
    end

    @page_info = OsVersionInfo.where(:key=>params[:app_id], :day_date=>@day, :login_user_count.exists=>true).
        order_by(:login_user_count, :desc).paginate(:per_page => 5)
  end

  #Os详细信息分页
  def os_details_page

    @day = DateUtils.today-1.day

    unless params[:day].nil?
      @day = DateUtils.format_day params[:day]
    end

    @page_info = OsVersionInfo.where(:key=>params[:app_id], :day_date=>@day, :login_user_count.exists=>true).
        order_by(:login_user_count, :desc).paginate(:per_page => 20, :page=>params[:page])
  end
end

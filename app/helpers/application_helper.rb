#encoding=utf-8
module ApplicationHelper
  def calc_percentage_to_s num1, num2, smth=2
    CalcUtils.calc_percentage_to_s num1, num2, smth
  end

  def calc_percentage_revert_to_s num1, num2, smth=2
    CalcUtils.calc_percentage_revert_to_s num1, num2, smth
  end

  #格式化日期
  #2011-1-1
  def format_date day
    DateUtils.format_date_to_string day
  end

  #格式化日期显示天
  #1
  def format_date_show_day day
    "#{day.day}"
  end

  #格式化字符
  def unknown_string string
    string = "Unknown" if string.blank?
    string
  end

  #通过uuid应用名字
  def app_name uuid
    product = Product.where(:_id=>uuid).first
    unless product.nil?
      return product.description
    end
    "该UUID没有找到对应的项目"
  end

  def concat_string string1, string2
    DataUtils.concat_string(string1, string2)
  end


end

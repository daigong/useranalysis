#encoding=utf-8
class ResponseTimeInfo
  include Mongoid::Document
  set_database :log
  store_in :response_time
  key :day_date, :type=>Time
  key :view, :type=>Hash
  key :busline, :type=>Hash
  key :busstop, :type=>Hash
  key :poi_name, :type=>Hash
  key :poi_around, :type=>Hash
  key :poi_area, :type=>Hash
  key :poi_city, :type=>Hash
  key :rg, :type=>Hash
  key :travel_walk, :type=>Hash
  key :travel_bus, :type=>Hash
  key :eeye, :type =>Hash
  key :mm, :type=>Hash


  def self.get_rg_view_day_info day
    today_info = ResponseTimeInfo.where(:day_date=>day).first
    data = {}
    view = {}
    rg = {}
    unless today_info.nil?
      view_mintime = {}
      view_avgtime = {}
      view_maxtime = {}
      view_count = {}
      (0..7).each do |i|
        level = "level".concat(i.to_s)
        view_mintime[level] = today_info.view[i.to_s]["min"]
        view_avgtime[level] = today_info.view[i.to_s]["avg"]
        view_maxtime[level] = today_info.view[i.to_s]["max"]
        view_count[level] = today_info.view[i.to_s]["count"]
      end
      view["min"] = view_mintime
      view["avg"] = view_avgtime
      view["max"] = view_maxtime
      view["count"] = view_count
      data["view"] = view

      rg_mintime = {}
      rg_avgtime = {}
      rg_maxtime = {}
      rg_count = {}
      rg_mintime["100km以下"] = today_info.rg["0"]["min"]
      rg_mintime["100km-500km"] = today_info.rg["1"]["min"]
      rg_mintime["500km-1000km"] = today_info.rg["2"]["min"]
      rg_mintime["1000km-3000km"] = today_info.rg["3"]["min"]
      rg_mintime["3000km以下"] = today_info.rg["4"]["min"]

      rg_avgtime["100km以下"] = today_info.rg["0"]["avg"]
      rg_avgtime["100km-500km"] = today_info.rg["1"]["avg"]
      rg_avgtime["500km-1000km"] = today_info.rg["2"]["avg"]
      rg_avgtime["1000km-3000km"] = today_info.rg["3"]["avg"]
      rg_avgtime["3000km以下"] = today_info.rg["4"]["avg"]

      rg_maxtime["100km以下"] = today_info.rg["0"]["max"]
      rg_maxtime["100km-500km"] = today_info.rg["1"]["max"]
      rg_maxtime["500公里-1000km"] = today_info.rg["2"]["max"]
      rg_maxtime["1000km-3000km"] = today_info.rg["3"]["max"]
      rg_maxtime["3000km以下"] = today_info.rg["4"]["max"]

      rg_count["100km以下"] = today_info.rg["0"]["count"]
      rg_count["100km-500km"] = today_info.rg["1"]["count"]
      rg_count["500公里-1000km"] = today_info.rg["2"]["count"]
      rg_count["1000km-3000km"] = today_info.rg["3"]["count"]
      rg_count["3000km以下"] = today_info.rg["4"]["count"]

      rg["min"] = rg_mintime
      rg["avg"] = rg_avgtime
      rg["max"] = rg_maxtime
      rg["count"] = rg_count
      data["rg"] = rg
    end
    return data

  end

  
  def self.get_day_info day

    mintime = {"busline"=>0, "busstop"=>0, "poi_name"=>0, "poi_around"=>0, "poi_area"=>0, "poi_city"=>0, "travel_bus"=>0,
        "travel_walk"=>0, "mm"=>0, "eeye"=>0}
    avgtime = {"busline"=>0, "busstop"=>0, "poi_name"=>0, "poi_around"=>0, "poi_area"=>0, "poi_city"=>0, "travel_bus"=>0,
        "travel_walk"=>0, "mm"=>0, "eeye"=>0}
    maxtime = {"busline"=>0, "busstop"=>0, "poi_name"=>0, "poi_around"=>0, "poi_area"=>0, "poi_city"=>0, "travel_bus"=>0,
        "travel_walk"=>0, "mm"=>0, "eeye"=>0}
    count = {"busline"=>0, "busstop"=>0, "poi_name"=>0, "poi_around"=>0, "poi_area"=>0, "poi_city"=>0, "travel_bus"=>0,
        "travel_walk"=>0, "mm"=>0, "eeye"=>0}
    data = {"avg"=>avgtime, "min"=>mintime, "max"=>maxtime, "count"=>count}
    today_info = ResponseTimeInfo.where(:day_date=>day).first
    unless today_info.nil?
      avgtime["busline"]=today_info.busline["avg"]
      avgtime["busstop"]=today_info.busstop["avg"]
      avgtime["poi_name"]=today_info.poi_name["avg"]
      avgtime["poi_around"]=today_info.poi_around["avg"]
      avgtime["poi_area"]=today_info.poi_area["avg"]
      avgtime["poi_city"]=today_info.poi_city["avg"]
      avgtime["travel_bus"]=today_info.travel_bus["avg"]
      avgtime["travel_walk"]=today_info.travel_walk["avg"]
      avgtime["mm"]=today_info.mm["avg"]
      avgtime["eeye"]=today_info.eeye["avg"]

      data["avg"] = avgtime
      mintime["busline"]=today_info.busline["min"]
      mintime["busstop"]=today_info.busstop["min"]
      mintime["poi_name"]=today_info.poi_name["min"]
      mintime["poi_around"]=today_info.poi_around["min"]
      mintime["poi_area"]=today_info.poi_area["min"]
      mintime["poi_city"]=today_info.poi_city["min"]
      mintime["travel_bus"]=today_info.travel_bus["min"]
      mintime["travel_walk"]=today_info.travel_walk["min"]
      mintime["mm"]=today_info.mm["min"]
      mintime["eeye"]=today_info.eeye["min"]
      data["min"] = mintime

      maxtime["busline"]=today_info.busline["max"]
      maxtime["busstop"]=today_info.busstop["max"]
      maxtime["poi_name"]=today_info.poi_name["max"]
      maxtime["poi_around"]=today_info.poi_around["max"]
      maxtime["poi_area"]=today_info.poi_area["max"]
      maxtime["poi_city"]=today_info.poi_city["max"]
      maxtime["travel_bus"]=today_info.travel_bus["max"]
      maxtime["travel_walk"]=today_info.travel_walk["max"]
      maxtime["mm"]=today_info.mm["max"]
      maxtime["eeye"]=today_info.eeye["max"]
      data["max"] = maxtime

      count["busline"]=today_info.busline["count"]
      count["busstop"]=today_info.busstop["count"]
      count["poi_name"]=today_info.poi_name["count"]
      count["poi_around"]=today_info.poi_around["count"]
      count["poi_area"]=today_info.poi_area["count"]
      count["poi_city"]=today_info.poi_city["count"]
      count["travel_bus"]=today_info.travel_bus["count"]
      count["travel_walk"]=today_info.travel_walk["count"]
      count["mm"]=today_info.mm["count"]
      count["eeye"]=today_info.eeye["count"]
      data["count"] = count
      
    end
    return data


  end
  
end


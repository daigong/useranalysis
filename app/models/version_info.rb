#encoding=utf-8
class VersionInfo
  include Mongoid::Document
  store_in :mobile_versioninfo
  key :key, :type => String
  key :day_date, :type => Time
  key :soft_version, :type => String
  key :sum_user_count, :type => Integer
  key :new_user_count, :type => Integer
  key :update_user_count, :type => Integer
  key :login_user_count, :type => Integer
  key :login_count,:type=>Integer
  #key :yest_login_count, :type=>Integer

  #实时获得各个版本的信息
  def self.all_version_info key, day
    VersionInfo.where(:day_date => day, :key => key).order_by(:soft_version, :desc);
  end

  def yest_login_count
    version_info = VersionInfo.where(:day_date => self[:day_date]-1.day, :key => self.key).first
    if version_info.nil?
      return 0
    else
      version_info[:login_user_count]
    end
  end
end


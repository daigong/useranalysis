#encoding=utf-8
class LogSumInfo
  include Mongoid::Document
  set_database :log
  store_in :sum
  key :day_date, :type=>Time
  key :view, :type=>Hash
  key :busline, :type=>Hash
  key :busstop, :type=>Hash
  key :poi, :type=>Hash
  key :poi_other, :type=>Hash
  key :bottle_configure, :type=>Hash
  key :version, :type=>Hash
  key :travel, :type=>Hash
  key :eeye, :type =>Hash
  key :mm, :type=>Hash
  key :other, :type=>Hash

  def self.get_percentage_day_info day
    sum_percentage_data = []
    today_info = LogSumInfo.where(:day_date=>day).first
    unless today_info.nil?
      one = ["view"]
      one<<today_info.view["avg"]
      sum_percentage_data<<one
      one = ["busline"]
      one<<today_info.busline["avg"]
      sum_percentage_data<<one
      one = ["busstop"]
      one<<today_info.busstop["avg"]
      sum_percentage_data<<one
      one = ["poi"]
      one<<today_info.poi["avg"]
      sum_percentage_data<<one
      one = ["poi_other"]
      one<<today_info.poi_other["avg"]
      sum_percentage_data<<one
      one = ["bottle_configure"]
      one<<today_info.bottle_configure["avg"]
      sum_percentage_data<<one
      one = ["version"]
      one<<today_info.version["avg"]
      sum_percentage_data<<one
      one = ["rg"]
      one<<today_info.rg["avg"]
      sum_percentage_data<<one
      one = ["travel"]
      one<<today_info.travel["avg"]
      sum_percentage_data<<one
      one = ["eeye"]
      one<<today_info.eeye["avg"]
      sum_percentage_data<<one
      one = ["mm"]
      one<<today_info.mm["avg"]
      sum_percentage_data<<one
      one = ["other"]
      one<<today_info.other["avg"]
      sum_percentage_data<<one
    end
    return sum_percentage_data
  end

  def self.get_count_day_info day
    sum_count_data = {}
    today_info = LogSumInfo.where(:day_date=>day).first
    unless today_info.nil?
      sum_count_data["view"]=today_info.view["count"]
      sum_count_data["busline"]=today_info.busline["count"]
      sum_count_data["busstop"]=today_info.busstop["count"]
      sum_count_data["poi"]=today_info.poi["count"]
      sum_count_data["poi_other"]=today_info.poi_other["count"]
      sum_count_data["bottle_configure"]=today_info.bottle_configure["count"]
      sum_count_data["version"]=today_info.version["count"]
      sum_count_data["rg"]=today_info.rg["count"]
      sum_count_data["travel"]=today_info.travel["count"]
      sum_count_data["eeye"]=today_info.eeye["count"]
      sum_count_data["mm"]=today_info.mm["count"]
      sum_count_data["other"]=today_info.other["count"]
    end
    return sum_count_data
  end

  def self.get_count_info begin_day, end_day
    temp_day=begin_day
    count_by_day={};
    count_by_day["view"]=[]
    count_by_day["busline"]=[]
    count_by_day["busstop"]=[]
    count_by_day["poi"]=[]
    count_by_day["poi_other"]=[]
    count_by_day["bottle_configure"]=[]
    count_by_day["version"]=[]
    count_by_day["rg"]=[]
    count_by_day["travel"]=[]
    count_by_day["eeye"]=[]
    count_by_day["mm"]=[]
    count_by_day["other"]=[]
    while temp_day<=end_day do
      sum_info = LogSumInfo.where(:day_date=>temp_day).first
      if sum_info.nil?
        count_by_day["view"]<<0
        count_by_day["busline"]<<0
        count_by_day["busstop"]<<0
        count_by_day["poi"]<<0
        count_by_day["poi_other"]<<0
        count_by_day["bottle_configure"]<<0
        count_by_day["version"]<<0
        count_by_day["rg"]<<0
        count_by_day["travel"]<<0
        count_by_day["eeye"]<<0
        count_by_day["mm"]<<0
        count_by_day["other"]<<0
      else
        count_by_day["view"]<<sum_info.view["count"]
        count_by_day["busline"]<<sum_info.busline["count"]
        count_by_day["busstop"]<<sum_info.busstop["count"]
        count_by_day["poi"]<<sum_info.poi["count"]
        count_by_day["poi_other"]<<sum_info.poi_other["count"]
        count_by_day["bottle_configure"]<<sum_info.bottle_configure["count"]
        count_by_day["version"]<<sum_info.version["count"]
        count_by_day["rg"]<<sum_info.rg["count"]
        count_by_day["travel"]<<sum_info.travel["count"]
        count_by_day["eeye"]<<sum_info.eeye["count"]
        count_by_day["mm"]<<sum_info.mm["count"]
        count_by_day["other"]<<sum_info.other["count"]
      end
        temp_day+=1.day
    end
    return count_by_day
    
  end
end
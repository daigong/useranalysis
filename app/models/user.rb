class User
  include Mongoid::Document
  store_in :mobile_terminals
  field :key, :type => String
  field :channel, :type => String
  field :uuid, :type => String
  field :first_login_time, :type => Time
  field :last_login_time, :type => Time
  field :os_version, :type => String
  field :soft_version, :type => String
  field :soft_version_code, :type => Integer
  field :device_model, :type => String
  field :resolution, :type => String
  field :sys_country, :type=>String
  field :carrier, :type => String
  field :connect_type, :type => String
  field :connect_subtype, :type => String
  field :cellid, :type=>String
  field :bssid, :type=>String
  field :last_login_lat, :type=>String
  field :last_login_lon, :type=>String
  field :last_login_province, :type => String
  field :last_login_city, :type => String
  field :prison,:type=>Boolean

  field :history_update, :type=>Array

  field :history_login_time, :type=>Array


end

#encoding=utf-8
class LogBuslineInfo
  include Mongoid::Document
  set_database :log
  store_in :busline
  key :day_date, :type=>Time
  key :province, :type=>String
  key :city, :type=>String
  key :count, :type=>Integer

end
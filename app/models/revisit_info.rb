#encoding=utf-8
class RevisitInfo
  include Mongoid::Document
  store_in :mobile_revisitinfo
  
  #新增用户
  key :new_user_count,:type=>Integer
  #7天回访客户
  key :week_user_count, :type=>Integer
  #14天回访客户
  key :dweek_user_count ,:type=>Integer
  
  key :day_date,:type=>Time
  key :key,:type=>String

end
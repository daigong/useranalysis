#encoding=utf-8
class CarrierInfo
  include Mongoid::Document
  store_in :mobile_carrierinfo

  key :key, :type=>String
  key :day_date, :type=>Time
  key :new_user_count, :type=>Integer
  key :login_user_count, :type=>Integer
  key :carrier, :type=>String
end


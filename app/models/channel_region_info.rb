#encoding=utf-8
class ChannelRegionInfo
  include Mongoid::Document
  store_in :mobile_regioninfo_channel
  key :key, :type=>String
  key :day_date, :type=>Time
  key :channel, :type=>String
  key :new_user_count, :type=>Integer
  key :province, :type=>String

  def self.get_total_by_day day, chan, app_key, page
    region_info = {};
    regions = ChannelRegionInfo.where(:day_date=>day, :key=>app_key, :channel=>chan).order_by(:new_user_count, :desc).paginate(:per_page => 5,:page=>page)
    regions.each do |d|
      if d.province.eql? ""
        region_info["UNKNOW"]=d.new_user_count
      else
        region_info[d.province]=d.new_user_count
      end
    end
    return region_info;
  end
end
#encoding=utf-8
class UpdateInfo
  include Mongoid::Document
  store_in :mobile_updateinfo

  key :key, :type=>String
  key :uuid, :type=>String
  key :time, :type=>Time
  key :history, :type=>Integer
  key :target, :type=>Integer
end
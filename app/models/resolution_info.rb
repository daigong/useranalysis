#encoding=utf-8
class ResolutionInfo
  include Mongoid::Document
  store_in :mobile_resolutioninfo

  key :key, :type=>String
  key :day_date, :type=>Time
  key :new_user_count, :type=>Integer
  key :sum_user_count, :type=>Integer
  key :resolution, :type=>String
end
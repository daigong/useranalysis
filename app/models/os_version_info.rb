#encoding=utf-8
class OsVersionInfo
  include Mongoid::Document
  store_in :mobile_osversioninfo

  key :key, :type=>String
  key :day_date, :type=>Time
  key :new_user_count, :type=>Integer
  key :login_user_count, :type=>Integer
  key :os_version, :type=>String
end
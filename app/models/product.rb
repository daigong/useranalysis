#encoding=utf-8
class Product
  include Mongoid::Document
  set_database :misc
  store_in :products
  key :name, :type=>String
  key :description, :type=>String
  key :_id, :type=>String
  key :is_active,:type=>Boolean
  #该产品的用户总数
  def day_info day
    BaseInfo.get_day_info(self._id, day)
  end

end
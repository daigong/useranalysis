#encoding=utf-8
class ChannelVersionInfo
  include Mongoid::Document
  store_in :mobile_versioninfo_channel
  key :key, :type=>String
  key :day_date, :type=>Time
  key :channel, :type=>String
  key :new_user_count, :type=>Integer
  key :soft_version, :type=>String

  def self.get_total_by_day day, chan, app_key, page
    version_info = {};
    versions = ChannelVersionInfo.where(:day_date=>day, :key=>app_key, :channel=>chan).order_by(:new_user_count, :desc).paginate(:per_page => 5,:page=>page)
    versions.each do |d|
      if d.soft_version.eql? ""
        version_info["UNKNOW"]=d.new_user_count
      else
        version_info[d.soft_version]=d.new_user_count
      end
    end
    return version_info;
  end
end
#encoding=utf-8
class RegionInfo
  include Mongoid::Document
  store_in :mobile_regioninfo

  key :key, :type=>String
  key :day_date, :type=>Time
  key :new_user_count, :type=>Integer
  key :login_user_count, :type=>Integer
  key :province, :type=>String
end
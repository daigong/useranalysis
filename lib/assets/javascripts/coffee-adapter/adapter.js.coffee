#CoffeeAdapterConfig
class CoffeeAdapterConfig

    #@classes所有的类都缓存在这里
    constructor: () ->
        @classes = {};
        return

    # 增加一个类
    # @param classNameWithNameSpace 完整类名：包名/命名空间.类名
    # @param classFunction 类函数
    #
    addPageClass: (classNameWithNameSpace, classFunction) ->
        @classes[classNameWithNameSpace] = classFunction
        return

    # 获得一个类
    # @param classNameWithNameSpace 类名字
    getPageClass: (classNameWithNameSpace) ->
        @classes[classNameWithNameSpace]
        
    # 获得Class 名字
    # @param fn Class function
    getClassName: (fn) ->
        if fn.name?
          return fn.name
        name = /\W*function\s+([\w\$]+)\(/.exec(fn);
        if !name
            log.error("没有找到函数名字:" + fn.toString());
            return 'no name';
        return name[1];
    #清除
    clear: () ->
        delete @classes

    version:'0.1.0'

window._coffeeAdapterConfig = new CoffeeAdapterConfig();

# 注册函数
# @param pageClass 类function对象
# @param namespace 命名空间/包名
window.registerClass = (pageClass, namespace) ->
    class_name = window._coffeeAdapterConfig.getClassName(pageClass)
    if (!namespace)
      log.warn("注册 #{class_name} 时没有指定namespace！")
      name_with_namespace = class_name
    else
      name_with_namespace = namespace + "." + class_name

    if (window._coffeeAdapterConfig.getPageClass(name_with_namespace))
      log.warn("类: #{name_with_namespace} 已经在其他地方定义，请确认！")
    else
      window._coffeeAdapterConfig.addPageClass(name_with_namespace, pageClass)
      log.info("类： #{name_with_namespace } 注册成功 ！")

#获得Class Function
window.$Class = () ->
    log.debug "查找class：#{arguments[0]}"
    clazz = window._coffeeAdapterConfig.getPageClass.apply(window._coffeeAdapterConfig, arguments)
    if (!clazz)
        message = "没有找到：" + arguments[0] + "类，请确认是否在使用前注册了该类？";
        log.error(message+" 请查看异常。");
        throw TypeError(message);
    return clazz
    
#页面Base Class，期待所有的页面类继承该类
class PageBase
  constructor:()->return
  init:() ->
    message = "请覆盖PageBase类的初始化方法为页面进行初始化！"
    log.error(message+"，请查看异常信息");
    throw TypeError(message)

#注册页面基本类
window.registerClass PageBase

$ ->
    $body = $('body');
    pageClass = $body.attr 'data-page-class';
    if (!pageClass)
        log.error("该页面body中没有找到data-page-class属性。")
        return;
    pageClassFunction = window._coffeeAdapterConfig.getPageClass(pageClass);
    if (!pageClassFunction)
        log.error("未找到页面body[data-page-class]配置的类: " + pageClass);
        return;
    pageParams = $body.attr 'data-page-params'
    params = [];
    #如果页面给定的params不是空
    if pageParams and pageParams !=''
      eval("params = #{pageParams}");
    log.debug "页面配置的body[data-page-class]为 #{pageClass}，页面初始化参数为 #{params}，开始初始化..."
    window._pageObject = new pageClassFunction();
    if (window._pageObject.init)
        window._pageObject.init.apply(window._pageObject,params);
        log.info("页面body[data-page-class]配置的类:#{pageClass},页面初始化参数为 #{params} 成功初始化。")
    else
        log.error("页面中配置的body[data-page-class]为： #{pageClassFunction } ，但是没有提供init方法。")

    window._coffeeAdapterConfig.clear();
    return
#encoding=utf-8
class DateUtils

  #获得当天 时间为00:00
  def self.today
    date_today = Date.today
    day = Time.gm(date_today.year, date_today.month, date_today.day)
  end

  def self.format_day strday
    date = Time.parse strday
    Time.gm(date.year, date.month, date.day)
  end

  #获得2天之间的所有天（包括这俩天,要求00：00）的字符数组
  #get_day_string(today,today-7.day)
  #return [2-27,2-28,3-1,3-2.....]
  def self.get_day_string begin_day, end_day
    day_string_array = []
    while begin_day<=end_day
      day_string_array<<"#{begin_day.month}-#{begin_day.day}"
      begin_day+=1.day
    end
    day_string_array
  end

  #格式化日期
  #2011-1-1
  def self.format_date_to_string day
    "#{day.year}-#{day.month}-#{day.day}"
  end


end
